# Archivos de impresión 3D para casa domotica.

Se incluyen 3 carpetas con los archivos necesarios para armar la casa domotica:

### Archivos STL para imprimir en 3D

Se incluyen los diseños para imprimir en impresora 3D

### Casa. DXF para imprimir en 3D

Se incluyen los diseños para hacer cortes laser en madera.

### Ensamblajes para armar todo

Video instructivo del ensamble.

